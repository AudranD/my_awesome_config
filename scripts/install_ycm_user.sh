P=/usr/lib/YouCompleteMe
U=~/.cache/vimfiles/repos/github.com/Valloric

mkdir -p "$U"

if [ -e "$U/YouCompleteMe" ]; then
    rm -rf "$U/YouCompleteMe"
fi

ln -s "$P" "$U/YouCompleteMe"
