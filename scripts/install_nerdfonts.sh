TEMP="$(mktemp -d)"

cd "$TEMP"
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
cd nerd-fonts
sudo ./install.sh -S
