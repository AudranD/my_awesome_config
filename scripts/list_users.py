with open('/etc/passwd', 'r') as f:
    for v in f.readlines():
        data = v[:-1].split(":")

        if data[-1] in {"/bin/zsh", "/bin/bash", "/usr/bin/fish", "/usr/bin/zsh", "/usr/bin/bash", "/usr/bin/mac-shell"}:
            print(data[0])
