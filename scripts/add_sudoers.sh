#!/bin/bash

if [ "$(cat /etc/sudoers | grep mac_user | wc -l)" -gt 0 ]; then
    exit 0
fi

chmod +w /etc/sudoers
echo "# My Awesome Config installation user (do not remove)
mac_user ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
chmod -w /etc/sudoers
