P=~/.cache/vimfiles/repos/github.com/Valloric
P=/usr/lib/YouCompleteMe

if [ ! -e "$P" ]; then
    git clone https://github.com/Valloric/YouCompleteMe.git "$P" --depth 1
    cd "$P"
    git submodule update --init --recursive
fi

cd "$P"

./install.py --all
