.PHONY: install
install:
	sudo mkdir -p /usr/lib/my_awesome_config
	sudo rm -rf /usr/lib/my_awesome_config/*
	sudo cp -R bin/ files/ scripts/ install.py config.json /usr/lib/my_awesome_config/
	sudo cp bin/* /usr/bin
