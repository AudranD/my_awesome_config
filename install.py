import os
import sys
import optparse
import json
import pwd
import subprocess
import tempfile

BASH    = "/bin/bash"
USERADD = "/usr/bin/useradd"

PACMAN  = "/usr/bin/pacman"
YAY     = "/usr/bin/yay"
APT     = "/usr/bin/apt"

def run_cmd(L):
    return subprocess.run(L).returncode

def normal_cmd(*cmd):
    return run_cmd(list(cmd))

def user_cmd(options, *cmd):
    L = list(cmd)

    if os.getuid() != options.user:
        L = ["sudo", "-u", "#" + str(options.user)] + L

    return run_cmd(L)

def root_cmd(*cmd):
    L = ["sudo"] + list(cmd)

    return run_cmd(["sudo"] + list(cmd))

def switch_install_user():
    normal_cmd(USERADD, "-M", "-s", "/bin/false", "mac_user")
    normal_cmd(BASH, "./scripts/add_sudoers.sh")

    uid = pwd.getpwnam("mac_user").pw_uid
    os.setuid(uid)

    home = tempfile.mktemp(suffix="home")
    os.environ["HOME"] = home

class PacmanInstall:
    def process(self, options, name):
        return root_cmd(PACMAN, "--noconfirm", "--needed", "-S", name)

    def allow_action(self, options):
        return options.global_scope() and os.path.exists(PACMAN)

class PacmanRemove:
    def process(sef, options, name):
        return root_cmd(PACMAN, "--noconfirm", "-R", name)

    def allow_action(self, options):
        return options.global_scope() and os.path.exists(PACMAN)

class YayInstall:
    def process(self, options, name):
        return normal_cmd(YAY, "--noconfirm", "--needed", "-S", name)

    def allow_action(self, options):
        return options.global_scope() and os.path.exists(YAY)

class Script:
    def process(self, options, name):
        return root_cmd(BASH, name)

    def allow_action(self, options):
        return options.global_scope() and os.path.exists(BASH)

class NormalScript:
    def process(self, options, name):
        return normal_cmd(BASH, name)

    def allow_action(self, options):
        return options.global_scope() and os.path.exists(BASH)

class UserScript:
    def process(self, options, name):
        return user_cmd(options, BASH, name)

    def allow_action(self, options):
        return options.user_scope() and os.path.exists(BASH)

managers = {
    'pacman_remove': PacmanRemove(),
    'pacman_install': PacmanInstall(),
    'yay_install': YayInstall(),
    'script': Script(),
    'user_script': UserScript(),
    'normal_script': NormalScript()
}

class Category:
    def __init__(self, m):
        assert "name" in m

        self.name = m["name"]
        self.config = m

    def has(self, options):
        for v in managers:
            if v not in self.config:
                continue

            L = self.config[v]
            M = managers[v]

            if M.allow_action(options):
                return True

        return False

    def install(self, options):
        for v in managers:
            if v not in self.config:
                continue

            L = self.config[v]
            M = managers[v]

            if not M.allow_action(options):
                continue

            for e in L:
                M.process(options, e)

class Options:
    def __init__(self):
        parser = optparse.OptionParser()

        parser.add_option("-s", "--scope", dest="scope", default="all", help="install only a specific scope (all, user, global)")
        parser.add_option("-u", "--user", dest="user", help="install user-specific files for specified user")
        parser.add_option("-n","--noconfirm", action="store_true", dest="noconfirm", default=False, help="install without confirmation")

        (options, args) = parser.parse_args()

        if options.scope not in {"all", "user", "global"}:
            print("unknown scope:", options.scope)
            sys.exit(1)

        self.scope = options.scope
        self.noconfirm = options.noconfirm
        self.user = options.user

        if self.user is not None:
            try:
                self.user = pwd.getpwnam(self.user).pw_uid
            except:
                print("unknown user:", self.user)
                sys.exit(1)
        else:
            self.user = os.getuid()

    def global_scope(self):
        return self.scope in {"all", "global"}

    def user_scope(self):
        return self.scope in {"all", "user"}

    def confirm(self, category):
        if self.noconfirm:
            return True

        ask = "Install " +  category.name + "? (Y/n) "

        while True:
            v = input(ask)

            if v == "Y" or v == "y" or v == "":
                return True
            elif v == "n" or v == "N":
                return False

if __name__ == "__main__":
    options = Options()

    if options.global_scope() and not os.getuid() == 0:
        print('Global scope can only be installed with root')
        sys.exit(1)

    if options.user_scope() and os.getuid() not in {0, options.user}:
        print('Cannot process user scope for specified user')
        sys.exit(1)

    if os.getuid() == 0:
        switch_install_user()

    with open('config.json', 'r') as f:
        config = json.loads(f.read())

    assert "categories" in config

    for cat in config["categories"]:
        cat = Category(cat)

        if cat.has(options) and options.confirm(cat):
            cat.install(options)
