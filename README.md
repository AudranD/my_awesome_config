# Installation

1. Clone this repository
2. Run `sudo make install`
3. Run `mac-install --scope global --noconfirm`
4. Run `mac-update --noconfirm`

# Configuration

Per-user configuration is in `~/.config/mac/config.yaml`

**Before updating youre configuration, check that it is valid by using the command `mac-template`.**

## xrandr confiruration

**command** (default: empty) custom xrand configuration command

## i3 configuration

**principal-key** (default: Mod4)

**secondary-key** (default: Mod1)


### Keyboard Speed

*To activate the keyboard speed modifier, you have to specify the following fields*:

**keyboard-speed.delay**: delay of key pressure before repetition
**keyboard-speed.rate**: number of repetitions per seconds

## Polybar configuration

**hwmon-path** (default: empty) path for temporary retrieval


## Configuration example

```yaml
xrandr:
    command: xrandr --my-params
i3:
    principal-key: Mod4
    secondary-key: Mod1
    keyboard-speed:
        delay: 130
        rate: 70
polybar:
    hwmon-path: /my/awesome/path
```
