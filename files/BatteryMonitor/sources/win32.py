from ctypes import Structure, wintypes, POINTER, windll, WinError, pointer, WINFUNCTYPE


GetSystemPowerStatus = None
try:
    GetSystemPowerStatus = windll.kernel32.GetSystemPowerStatus

    class SYSTEM_POWER_STATUS(Structure):
        _fields_ = [
            ('ACLineStatus', wintypes.c_ubyte),
            ('BatteryFlag', wintypes.c_ubyte),
            ('BatteryLifePercent', wintypes.c_ubyte),
            ('Reserved1', wintypes.c_ubyte),
            ('BatteryLifeTime', wintypes.DWORD),
            ('BatteryFullLifeTime', wintypes.DWORD)
            ]

    GetSystemPowerStatus.argtypes = [POINTER(SYSTEM_POWER_STATUS)]
    GetSystemPowerStatus.restype = wintypes.BOOL
except AttributeError as e:
    raise RuntimeError("Unable to load GetSystemPowerStatus."
                       "The system does not provide it (Win XP is required) or kernel32.dll is damaged.")

        power_status = SYSTEM_POWER_STATUS()
        if not GetSystemPowerStatus(pointer(power_status)):
            raise WinError()
