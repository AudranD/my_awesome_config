import threading, time
from batterystatus import BatteryState, BatteryStatus, Monitor

path = "/sys/class/power_supply/BAT%s/uevent"

def readUEventFile(battery):
    file = path % battery
    res = {}

    with open(file, 'r') as f:
        for line in f:
            key, value = line.split('=', 1)
            res[key] = value[0:-1]

    return res

power = ['POWER_SUPPLY_CURRENT_NOW', 'POWER_SUPPLY_POWER_NOW']

charge = ['POWER_SUPPLY_CHARGE_NOW', 'POWER_SUPPLY_ENERGY_NOW']
chargeF = ['POWER_SUPPLY_CHARGE_FULLL', 'POWER_SUPPLY_ENERGY_FULL']

status = ['POWER_SUPPLY_STATUS']
capacity = ['POWER_SUPPLY_CAPACITY']

def getEnergy(res):
    for i in range(len(charge)):
        pw, pwm = charge[i], chargeF[i]

        if pw in res:
            r1 = res[pw]
            r2 = res[pwm] if (pwm in res) else r1

            return int(r1), int(r2)

    raise Exception('Invalid uevent: missing energy')

def getPower(res):
    for en in power:
        if en in res:
            return int(res[en])

    raise Exception('Invalid uevent: missing power')

def getCapacity(res):
    for cp in capacity:
        if cp in res:
            return int(res[cp])

    raise Exception('Invalid uevent: missing capacity')

def statusFromString(statusStr):
    if statusStr == 'Discharging' or statusStr == 'Not charging':
        state = BatteryState.DISCHARGING
    elif statusStr == 'Charging':
        state = BatteryState.CHARGING
    elif statusStr == 'Unknown':
        state = BatteryState.UNKNOWN
    elif statusStr == 'Full':
        state = BatteryState.FULLY_CHARGED
    else:
        raise Exception('Invalid status: %s' % statusStr)

    return state

def getStatus(res):
    for st in status:
        if st in res:
            return statusFromString(res[st])

    raise Exception('Invalid uevent: missing status')

def getBatteryInformations(battery):
    values = readUEventFile(battery)

    state = getStatus(values)
    powerNow = getPower(values)
    energyNow, energyMax = getEnergy(values)

    if 'POWER_SUPPLY_CAPACITY' in values:
        perc = int(values['POWER_SUPPLY_CAPACITY'])
    else:
        perc = int((energyNow / energyMax) * 100)

    if state.isCharging():
        energyNow = energyMax - energyNow

    if powerNow == 0:
        remaining = 0
    else:
        remaining = int(3600 * (int(energyNow) / int(powerNow)))

    return perc, state, remaining

def updateStatus(battery, status):
    perc, state, remaining = getBatteryInformations(battery)
    updated = False

    updated = status.updateState(state)
    updated = status.updatePerc(perc) or updated
    updated = status.setRemaining(remaining) or updated

    return updated

class UEventMonitor(Monitor):
    def __init__(self, battery, callback):
        self.callback = callback
        self.battery = battery
        self.status = BatteryStatus()

        updateStatus(battery, self.status)

    def getStatus(self):
        return self.status

    def __monitor(self):
        while True:
            time.sleep(0.3)

            if updateStatus(self.battery, self.status):
                self.callback(self.getStatus())

    def createMonitor(self):
        thread = threading.Thread(target = self.__monitor)
        thread.setDaemon(True)
        thread.start()
