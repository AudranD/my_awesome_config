from batterystatus import BatteryState, BatteryStatus, Monitor

import dbus
from dbus import glib

dest = "org.freedesktop.UPower"
path = "/org/freedesktop/UPower/devices/battery_BAT%s"
iface = "org.freedesktop.DBus.Properties"

sbus = dbus.SystemBus()

class UPowerMonitor(Monitor):
	def __init__(self, batteryName, callback):
		self.callback = callback
		self.status = BatteryStatus()

		battery = sbus.get_object(dest, path % batteryName)
		self.iface = dbus.Interface(battery, iface)
		self.batteryName = batteryName

		self.updateStatus(self.iface.GetAll("org.freedesktop.UPower.Device"))

	def updateStatus(self, values):
		keys = values.keys()
		updated = False

		status = self.status

		if 'State' in keys:
			updated = status.updateState(BatteryState(values['State']))

		if 'Percentage' in keys:
			updated = status.updatePerc(int(values['Percentage'])) or updated

		if 'TimeToFull' in keys and status.state.isCharging():
			self.status.setRemaining(values['TimeToFull'])
			updated = True

		if 'TimeToEmpty' in keys and not status.state.isCharging():
			self.status.setRemaining(values['TimeToEmpty'])
			updated = True

		return updated

	def getStatus(self):
		return self.status

	def onBatteryChange(self, *args, **kwargs):
		changed = self.updateStatus(args[1])

		if self.updateStatus:
			self.callback(self.getStatus())

	def createMonitor(self):
		sbus.add_signal_receiver(self.onBatteryChange,
			signal_name="PropertiesChanged",
			dbus_interface=iface, path=path % self.batteryName)
