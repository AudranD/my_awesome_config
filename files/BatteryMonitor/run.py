#!/usr/bin/env python3
import signal
import sys, getopt
import os

import batterystatus, indicator

from gi.repository import GObject
from gi.repository import Gtk

TEST_MODE = False
TESTED_STATE = batterystatus.BatteryState.CHARGING

GObject.threads_init()

def batteryStatus(status):
    print(status)
    indicator.updateStatus(status)

def main(battery):
    l = GObject.MainLoop()

    if TEST_MODE:
        monitor = batterystatus.TestMonitor(False, batteryStatus)
    else:
        monitor = batterystatus.createMonitor(battery, batteryStatus)

    if monitor is None:
        print("FatalError: can't create monitor")
    else:
        monitor.startMonitor()
        l.run()



def get_default_battery(rng):
    for i in rng:
        if os.path.exists(f"/sys/class/power_supply/BAT{i}"):
            return i

    return 0


if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    argv = sys.argv[1:]

    try:
        opts, args = getopt.getopt(argv, "hb", ["help", "battery=", "test"])
    except getopt.GetoptError:
        print(sys.argv[0], '(-b <batteryId>)')
        sys.exit(2)

    battery = get_default_battery(range(0, 10))

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(sys.argv[0], '(-b <batteryId>) (--test)')
            print(sys.argv[0], '(--battery=<batteryId>) (--test)')
            sys.exit(0)

        if opt == '--test':
            TEST_MODE = True

        if opt in ('-b', '--battery'):
            battery = int(arg)

    print('Use battery:', battery, '| Test mode:', TEST_MODE)
    main(battery)
