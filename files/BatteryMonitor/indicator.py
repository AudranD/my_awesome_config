import gi, sys
import notif

gi.require_version('AppIndicator3', '0.1')
from gi.repository import AppIndicator3, Gtk, GObject
from batterystatus import BatteryState

label = 'Battery Monitor'
indCat = AppIndicator3.IndicatorCategory.SYSTEM_SERVICES

def getIcon(status):
    if status is None:
        return notif.path + '/discharging0.svg'

    sName = 'discharging'

    if status.state.isCharging():
        sName, sId = 'charging', status.perc // 100
    elif status.perc <= 10:
        sId = 0
    else:
        sId = (status.perc - 1) // 20 + 1

    return notif.path + '/%s%i.svg' % (sName, sId)

def getHelp(status):
    if status is None:
        return '-'

    t = 'Charging' if status.state.isCharging() else 'Discharging'
    return '%i%% - %s\n%s remaining' % (status.perc, t, status.timeto)

class MyIndicator:
    def __init__(self):
        self.current_icon = getIcon(None)
        self.current_help = None
        self.current_state = None
        self.current_perc = 100

        self.ind = AppIndicator3.Indicator.new(label, self.current_icon, indCat)
        self.displayed = False

        self.createMenu()

    def display(self):
        self.ind.set_status(AppIndicator3.IndicatorStatus.ACTIVE)
        self.displayed = True

    def updateStatus(self, status):
        icon = getIcon(status)
        help = getHelp(status)

        if icon != self.current_icon:
            self.current_icon = icon
            self.__menuIcon(icon)

            GObject.idle_add(self.ind.set_icon, icon,
                priority = GObject.PRIORITY_DEFAULT)    

        if help != self.current_help:
            self.current_help = help
            GObject.idle_add(self.imoni.set_label, help,
                priority = GObject.PRIORITY_DEFAULT)

        hntf = False

        if status.state != self.current_state:
            self.current_state = status.state
            hntf = notif.setStateNotif(status)
            hntf = not status.state.isCharging() and notif.setPercNotif(101, status) or hntf

        if status.perc != self.current_perc:
            if status.state == BatteryState.DISCHARGING:
                hntf = notif.setPercNotif(self.current_perc, status) or hntf

            self.current_perc = status.perc

        if not self.displayed:
            self.display()

        if hntf:
            notif.display()

    def __menuIcon(self, icon):
        ic = Gtk.Image()
        ic.set_from_file(icon)

        GObject.idle_add(self.imoni.set_image, ic,
            priority = GObject.PRIORITY_DEFAULT)


    def createMenu(self):
        menu = Gtk.Menu()

        imoni = Gtk.ImageMenuItem('Informations')
        imoni.set_always_show_image(True)

        menu.append(imoni)
        self.imoni = imoni

        iquit = Gtk.MenuItem('Quit')
        iquit.connect("activate", self.__quit)

        menu.append(iquit)
        menu.show_all()

        self.ind.set_menu(menu)

    def __quit(self, _):
        sys.exit(0)

indicator = MyIndicator()
updateStatus = indicator.updateStatus
