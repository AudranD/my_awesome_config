import os, threading, time

import gi

from gi.repository import GLib

gi.require_version('Notify', '0.7')
from gi.repository import Notify, GObject
from batterystatus import BatteryState

Notify.init("Battery Monitor")

path = os.path.dirname(os.path.abspath(__file__)) + '/icons'

notif = Notify.Notification.new('', '', '')

notif.set_urgency(Notify.Urgency.CRITICAL)
notif.set_timeout(10000)
notif.add_action('quit', 'Quit', lambda _, __: None)

percs = [
	(10, ("{remaining} ({perc}%) remaining", "Your battery is extremly low ({perc}%), if you need to continue using your computer, plug in it.", "{path}/critical-battery.png")),
	(20, ("{remaining} ({perc}%) remaining", "Your battery is low ({perc}%), if you need to continue using your computer, plug in it.", "{path}/low-battery.png"))
]

states = {
	"charging": ("Charging: {remaining} ({perc}%)", "Your computer has been plugged in", "{path}/charging.png"),
	"discharging": ("Discharging: {remaining} ({perc}%)", "Your computer has been unplugged in", "{path}/not-charging.png"),
	"fully-charged": ("Fully charged ({perc}%)", "Your computer is fully charged. You can unplug it.", "{path}/full-charge.png" )
}

def setNotif(msg, status):
	head, body, icon = msg

	head = head.format(perc=status.perc, remaining=status.timeto)
	body = body.format(perc=status.perc, remaining=status.timeto)

	icon = icon.format(path=path)

	notif.update(head, body, icon)

def setPercNotif(old, status):
	m, mname = 100, None
	new = status.perc

	for value, name in percs:
		if new <= value and value < old and value <= m:
			m, mname = value, name

	if mname is not None:
		setNotif(mname, status)
		return True

	return False

def setStateNotif(status):
	res = None
	state = status.state

	if state == BatteryState.CHARGING:
		res = states["charging"]

	if state == BatteryState.FULLY_CHARGED:
		res = states["fully-charged"]

	if state == BatteryState.DISCHARGING:
		res = states["discharging"]

	if res is not None:
		setNotif(res, status)
		return True

	return False

def __notif():
	GObject.idle_add(notif.show, priority = GObject.PRIORITY_DEFAULT)
	#GObject.idle_add(notif.close, priority = GObject.PRIORITY_DEFAULT)

def display():
	try:
		notif.show()
	except Exception as e:
		print("can't notify:", e)
