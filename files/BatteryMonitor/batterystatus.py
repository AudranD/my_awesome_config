from enum import Enum
import random, time, threading

class BatteryState(Enum):
	UNKNOWN = 0
	CHARGING = 1
	DISCHARGING = 2
	EMPTY = 3
	FULLY_CHARGED = 4

	def isUnknown(self):
		return self == BatteryState.UNKNOWN

	def isCharging(self):
		return self == BatteryState.CHARGING or self == BatteryState.FULLY_CHARGED

class BatteryStatus:
	def __init__(self):
		self.state = BatteryState.UNKNOWN
		self.perc = 100
		self.timeto = ''

	def setRemaining(self, r):
		#FIXME
		r //= 60
		m = r % 60
		h = r // 60

		timeto = '{:02}h{:02}m'.format(h, m)

		if timeto == self.timeto:
			return False

		self.timeto = timeto
		return True

	def updateState(self, state):
		if state == BatteryState.UNKNOWN or state == self.state:
			return False

		self.state = state
		return True

	def updatePerc(self, perc):
		if perc < 0 or perc > 100 or perc == self.perc:
			return False

		self.perc = perc
		return True

	def __repr__(self):
		return "State: {s.state}, Percentage: {s.perc}, Time to: {s.timeto}".format(s = self)

class Monitor:
	callback = None

	def __init__(self):
		pass

	def getStatus(self):
		raise Exception('Not implemented')

	def monitor(self):
		raise Exception('Not implemented')

	def startMonitor(self):
		self.callback(self.getStatus())
		self.createMonitor()

class TestMonitor(Monitor):
	def __init__(self, charging, callback):
		self.callback = callback
		self.status = BatteryStatus()

		if charging:
			self.state = BatteryState.CHARGING
		else:
			self.state = BatteryState.DISCHARGING

	def getStatus(self):
		bStatus = self.status
		bStatus.state = self.state

		if self.state.isCharging():
			if random.randint(1, 4) <= 2:
				bStatus.perc = 100
			else:
				bStatus.perc = random.randint(1, 100)
		else:
			bStatus.perc += random.randint(5, 15)
			bStatus.perc %= 101

		return bStatus

	def __monitor(self):
		while True:
			time.sleep(1)
			self.callback(self.getStatus())

	def createMonitor(self):
		thread = threading.Thread(target = self.__monitor)
		thread.setDaemon(True)
		thread.start()

def createMonitor(battery, callback):
	try:
		print('Trying uevent monitor...')
		from sources import uevent
		return uevent.UEventMonitor(battery, callback)
	except Exception as e:
		print('uevent monitor failed:', e)

	try:
		print('Trying upower monitor...')
		from sources import upower
		return upower.UPowerMonitor(battery, callback)
	except Exception as e:
		print('upower monitor failed:', e)

	return None
