#!/bin/sh

if nvidia-smi 2> /dev/null >&2;
then
    nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader,nounits 2> /dev/null | awk '{ print "GPU",""$1"","%"}'
fi
