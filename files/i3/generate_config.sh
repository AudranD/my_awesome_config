#!/bin/sh

layout=$(setxkbmap -query | grep layout | rev | cut -d' ' -f1 | rev)

cd ~/.config/i3

if [[ "$1" != "--noreset" ]]; then
    rm -f .polybar_hidden
fi

if ! mac-template; then
    exit 1
fi

rm -f config

for v in main startup workspace_management theme;
do
    mac-template "config_dir/$v" >> config
done

if [[ "$layout" == "fr" ]];
then
    mac-template config_dir/layout_fr >> config
else
    mac-template config_dir/layout_us >> config
fi
