#!/usr/bin/fish

. ~/.config/fish/config.fish

set foc (xdotool getwindowfocus)
set pid (xdotool getwindowpid $foc)

set pid (pgrep -P $pid '^fish$')

set fish (pgrep -P $pid "fish")
set ranger (pgrep -P $pid "ranger")

if [ -z "$fish" ]
    if [ -n "$ranger" ]
        set fish $ranger
    else
        set fish $pid
    end
end

set dir (pwdx $fish | cut -d' ' -f2)

cd $dir
i3-sensible-terminal -u -e "$argv[1]"
