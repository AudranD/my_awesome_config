#!/bin/sh

layout=$(setxkbmap -query | grep layout | rev | cut -d' ' -f1 | rev)

if [[ "$layout" == "fr" ]];
then
    setxkbmap us
else
    setxkbmap fr
fi

sh ~/.config/i3/reload.sh
