import i3

def move_all():
    i = 1

    workspaces = [int(v['name']) for v in i3.get_workspaces() if v['name'].isnumeric()]
    workspaces.sort()

    for w in workspaces:
        if w != i:
            i3.command("rename workspace {0} to {1}".format(w, str(i)))
        i += 1

if __name__ == '__main__':
    move_all()
