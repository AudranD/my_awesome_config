import i3

def get_min_empty():
    i = 1

    workspaces = [int(v['name']) for v in i3.get_workspaces() if v['name'].isnumeric()]
    workspaces.sort()

    for c in workspaces:
        if c != i:
            return i

        i += 1

    return i

if __name__ == '__main__':
    i3.command('workspace {0}'.format( get_min_empty() ))
