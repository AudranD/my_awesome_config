cd ~/.config/i3/

if [ -f .polybar_hidden ];
then
    rm -f .polybar_hidden
    polybar-msg cmd restart
else
    touch .polybar_hidden
    polybar-msg cmd hide
fi
