fenv source /etc/profile

if [ $TERM = linux ] # TTY
    omf theme default
else
    omf theme budspencer
end

set -U fish_key_bindings fish_vi_key_bindings
set -U budspencer_nogreeting
set -U budspencer_no_cd_bookmark

alias sl=ls
alias perf_detail='perf -record -e cycles,instructions,cache-misses --call-graph dwarf'
alias k=kubectl

function vim
    if [ -n "$SSH_CLIENT" ]; or [ -n "$SSH_TTY" ]
        env DISPLAY= nvim $argv
    else
        nvim $argv
    end
end

set -x PYTHONSTARTUP ~/.config/init_python.py
set -x PATH ~/.cargo/bin $PATH

function lgrep
    grep --color=always $argv | less -r
end

bind -M insert \ca "commandline -f beginning-of-line"
bind -M insert \ce "commandline -f end-of-line"

function conda_init
    set __conda_setup (env CONDA_REPORT_ERRORS=false "$HOME/anaconda3/bin/conda" shell.bash hook 2> /dev/null)

    if [ $status -eq 0 ]
        \eval "$__conda_setup"
    else
        if [ -f "$HOME/anaconda3/etc/profile.d/conda.sh" ]
            . "$HOME/anaconda3/etc/profile.d/conda.sh"
            env CONDA_CHANGEPS1=false conda activate base
        else
            set -x PATH "$PATH:$HOME/anaconda3/bin"
        end
    end
    set -e __conda_setup
end

source ~/.fish_custom
