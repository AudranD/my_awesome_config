#!/bin/sh

export GTK_THEME=Adwaita:dark

# don't use dark theme on firefox
alias firefox='GTK_THEME= firefox'
