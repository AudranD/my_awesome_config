#!/bin/bash

CONFIRM=1
SCOPE=0

SCRIPT="sudo sh"
USCRIPT="sh"
UHOME="$HOME"

PACMAN="sudo pacman --noconfirm"
YAY="yay --noconfirm"

function ask()
{
    if [ "$SCOPE" -eq 1 -a "$1" = "global" ]; then
        return 1
    fi

    if [ "$SCOPE" -eq 2 -a "$1" = "user" ]; then
        return 1
    fi

    if [ "$CONFIRM" -gt "0" ]; then
        printf "Install $2? (Y/n) "
        read a

        if [ "$a" = "y" -o "$a" = "Y" -o -z "$a" ]; then
            return 0
        else
            return 1
        fi
    else
        echo "Install $2"
    fi
}

function install()
{
    if ask "global" "base packages"; then
        $PACMAN -S git
        $PACMAN -S openssh
        $PACMAN -S vim
        $PACMAN -S strace
        $PACMAN -S valgrind
        $PACMAN -S gdb
        $PACMAN -S gcc
        $PACMAN -S xdotool
        $PACMAN -S feh
        $PACMAN -S evince
        $PACMAN -S base-devel
        $PACMAN -S cmake
        $PACMAN -S fakeroot
        $PACMAN -S screen
        $PACMAN -S htop
        $PACMAN -S python3
        $PACMAN -S python-pip
        $PACMAN -S pulseaudio
        $PACMAN -S tree
    fi

    if ask "global" "useful packages (firefox, thunderbird, ...)"; then
        $PACMAN -S firefox
        $PACMAN -S thunderbird
        $PACMAN -S vlc
    fi

    if ask "global" "Yay"; then
        $SCRIPT ./scripts/install_yay.sh
    fi

    if ask "global" "useless firmwares"; then
        $YAY -S wd719x-firmware
        $YAY -S aic94xx-firmware
    fi

    if ask "global" "i3"; then
        $PACMAN -S xorg-server xorg-xinit xorg-apps notification-daemon
        $SCRIPT ./scripts/add_dbus_notf_deamon.sh
        $PACMAN -S i3
    fi

    if ask "global" "the one true i3lock"; then
        $PACMAN -R i3lock
        $YAY -S i3lock-epita
    fi

    if ask "global" "shutter (for screenshots)"; then
        $YAY -S shutter
    fi

    if ask "global" "zsh"; then
        $PACMAN -S zsh
    fi

    if ask "global" "Urxvt"; then
        $PACMAN -S rxvt-unicode
    fi

    if ask "global" "ZSH/Antigen"; then
        $SCRIPT ./scripts/install_antigen.sh
    fi

    if ask "global" "Complete Vim"; then
        $PACMAN -R vim
        $PACMAN -S gvim
    fi

    if ask "user" "SpaceVim"; then
        HOME="$UHOME" $USCRIPT - < ./scripts/install_spacevim.sh
    fi

    if ask "global" "YouCompleteMe (dependencies)"; then
        $PACMAN -S nodejs npm
        $PACMAN -S rust
        $PACMAN -S mono
    fi

    if ask "user" "YouCompleteMe (installation)"; then
        HOME="$UHOME" $USCRIPT - < ./scripts/install_ycm.sh
    fi

    if ask "global" "Ranger (dependencies)"; then
        $PACMAN -S ffmpegthumbnailer
        $PACMAN -S w3m
        $PACMAN -S transmission-cli
        $PACMAN -S odt2txt
        $PACMAN -S lynx
        $PACMAN -S elinks
        $PACMAN -S unrar
        $PACMAN -S p7zip
        $PACMAN -S libarchive
        $PACMAN -S perl-image-exiftool
        $PACMAN -S mediainfo
        $PACMAN -S pygmentize
        $PACMAN -S highlight
        $PACMAN -S cmake

        $YAY -S python-pdftotext
    fi

    if ask "global" "Ranger (installation)"; then
        $PACMAN -S ranger
    fi

    if ask "global" "Polybar (dependencies)"; then
        $PACMAN -S pasystray
    fi

    if ask "global" "Polybar (installation)"; then
        $YAY -S polybar
    fi

    if ask "global" "Battery monitor (dependencies)"; then
        $PACMAN -S gtk3 python-gobject libappindicator3
    fi

    if ask "global" "NerdFonts (~1Go)"; then
        $SCRIPT ./scripts/install_nerdfonts.sh
    fi

    if ask "user" "Configurations"; then
        RULE=all

        if [ "$SCOPE" -eq 1 ]; then
            RULE=user
        fi

        HOME="$UHOME" make generate
        HOME="$UHOME" $CUSER make -f - $RULE < Makefile
        make clean
    fi
}

while [ "$#" -gt 0 ]; do
    arg="$1"

    if [ "$arg" = "--noconfirm" ]; then
        CONFIRM=0
    elif [ "$arg" = "--confirm" ]; then
        PACMAN="sudo pacman"
        YAY="yay"
    elif [ "$arg" = "--user" ]; then
        shift

        if [ "$#" -eq 0 ]; then
            echo "--user: no user specified"
            exit 1
        fi

        USCRIPT="sudo -u $1 sh"
        CUSER="sudo -u $1"
        UHOME=$(eval echo ~$1)
    elif [ "$arg" = "--scope" ]; then
        shift

        if [ "$#" -eq 0 ]; then
            echo "--scope: no scope specified"
            exit 1
        elif [ "$1" = "user" ]; then
            SCOPE=1
        elif [ "$1" = "global" ]; then
            SCOPE=2
        elif [ "$1" = "all" ]; then
            SCOPE=0
        else
            echo "unknown scope: $1"
            exit 1
        fi
    elif [ "$arg" = "--help" ]; then
        echo "--noconfirm: install without confirmation"
        echo "--confirm: ask confirmation for pacman and yay"
        echo "--user <user>: install user-specific files for specified user"
        echo "--scope <user|global|all>: install only categories of a specific scope (default: all)"

        exit 0
    else
        echo "Unknown parameter: $arg"
        exit 1
    fi

    shift
done

install
