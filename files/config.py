import os
import json
import sys

def get_parent(path):
    path = os.path.join(path, os.path.pardir)

    return os.path.abspath(path)

def print_rule(name, depends):
    print('\n.PHONY:', name)
    print(name, ': ', depends, sep ='')

def get_scope(f):
    return f["scope"] if "scope" in f else "user"

def add_scope(M, scope, name):
    if scope not in M:
        M[scope] = []

    M[scope].append(name)

empty = len(sys.argv) > 1 and sys.argv[1] == '--empty'

with open('config.json', 'r') as f:
    config = json.loads(f.read())

files = config['files']

print('MKDIR = mkdir -p')
print('CP = cp -R')
print('RM = rm -rf')

print_rule('default', 'all')

print('\ngenerate:')
print('\tpython3 config.py > Makefile')

if not empty:
    M = {}

    for f in files:
        fr = f['from']
        to = os.path.expanduser(f['to'])
        pa = get_parent(to)

        prefix = f['prefix'] + " " if 'prefix' in f else ""

        name = f['name']

        add_scope(M, get_scope(f), name)

        print_rule(name, '')

        if fr[-1] != "*":
            print('\t${MKDIR}', pa)
            print('\t' + prefix + '${RM}   ', to)
        else:
            print('\t${MKDIR}', to)

        print('\t' + prefix + '${CP}   ', fr, to)

    for k in M:
        print_rule(k, ' '.join(M[k]))

    print_rule("all", ' '.join(M))
else:
    print('\nall: generate')

print('\nclean:')
print('\tpython3 config.py --empty > Makefile')
