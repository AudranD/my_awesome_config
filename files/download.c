#define _GNU_SOURCE

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <omp.h>

#include <curl/curl.h>

void download(const char *url, const char *name, const char *url_ext,
                const char *ext, unsigned id)
{
    char *v_url = NULL;
    char *v_dst = NULL;

    if (asprintf(&v_url, "%s%u%s", url, id, url_ext) < 0)
        err(1, "can't create url");

    if (asprintf(&v_dst, "%s%u%s", name, id, ext) < 0)
        err(1, "can't create destination");

    FILE *dst = fopen(v_dst, "w");

    if (!dst)
        err(1, "cant open destination file");

    CURLcode ret;
    CURL *hnd;

    hnd = curl_easy_init();
    curl_easy_setopt (hnd, CURLOPT_URL, v_url);
    curl_easy_setopt (hnd, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt (hnd, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 Vivaldi/2.2.1388.37");
    curl_easy_setopt (hnd, CURLOPT_MAXREDIRS, 50L);
    curl_easy_setopt (hnd, CURLOPT_TCP_KEEPALIVE, 1L);
    curl_easy_setopt (hnd, CURLOPT_WRITEDATA, dst);
    curl_easy_setopt (hnd, CURLOPT_SSL_VERIFYHOST, 0L);

    ret = curl_easy_perform (hnd);
    curl_easy_cleanup (hnd);

    if (ret == 0)
        printf("%s downloaded!\n", v_dst);
    else
        printf("[ERROR] can't download %s\n", v_dst);

    fclose(dst);
    free(v_url);
    free(v_dst);
}

void download_all(const char *url, const char *name, const char *url_ext,
                        const char *ext, unsigned min,
                        unsigned max, unsigned thread_count)
{
    if (thread_count < 1)
        thread_count = 1;

    omp_set_dynamic(0);
    omp_set_num_threads(thread_count);

    #pragma omp parallel for
    for (unsigned i = min; i <= max; i++)
    {
        download(url, name, url_ext, ext, i);
    }
}

int main(void)
{
    const char *url = "https://avatar-realms.cellar.services.clever-cloud.com/gallery/2";
    const char *url_ext = "/movies/streaming/VOSTFR.mp4";
    const char *name = "S2E";
    const char *ext = ".mp4";

    download_all(url, name, url_ext, ext, 15, 100, 4);
    return 0;
}
